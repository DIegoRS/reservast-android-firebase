package com.example.reservast.AdaptadorReservas;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.reservast.Menu;
import com.example.reservast.Principal;
import com.example.reservast.R;
import com.example.reservast.Reserva;
import com.example.reservast.entidades.reservaciones;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.firestore.firebase;
import com.example.reservast.ui.dashboard.DashboardFragment;
import com.example.reservast.ui.home.RestaurantesAdpatador;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class AdaptadorReservas extends RecyclerView.Adapter<AdaptadorReservas.ReservasViewHolder>{
    private ArrayList<reservaciones> data;
    private firebase firebase;

    public AdaptadorReservas(ArrayList<reservaciones> data) {
        this.data = data;
        firebase = new firebase();
        firebase.iniciarFirebase();

    }

    @Override
    public AdaptadorReservas.ReservasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdaptadorReservas.ReservasViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_reservas, parent, false));
    }

    @Override
    public void onBindViewHolder(AdaptadorReservas.ReservasViewHolder holder, int position) {
        reservaciones reservacion = data.get(position);
        Uri image=cargarImagen(reservacion.getImg());
        if(image!=null){
            Glide.with(holder.imgRestaurante.getContext())
                    .load(reservacion.getImg())
                    .apply(new RequestOptions().override(300, 200))
                    .into(holder.imgRestaurante);
        }
        holder.tvnombreRestaurante.setText(reservacion.getRestaurant());
        holder.tvDia.setText(reservacion.getDate());
        holder.tvHora.setText(reservacion.getHour());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ReservasViewHolder extends RecyclerView.ViewHolder{

        ImageView imgRestaurante;
        TextView tvnombreRestaurante;
        TextView tvDia;
        TextView tvHora;
        Button cancelaReserva;
        RatingBar calificacion;

        public ReservasViewHolder(View itemView) {
            super(itemView);
            imgRestaurante = (ImageView) itemView.findViewById(R.id.Imv_imagenReservas);
            tvnombreRestaurante = (TextView) itemView.findViewById(R.id.Tv_nombreReservas);
            tvDia= (TextView) itemView.findViewById(R.id.Tv_diaReservas);
            tvHora=(TextView) itemView.findViewById(R.id.Tv_horaReservas);
            cancelaReserva=(Button) itemView.findViewById(R.id.Btn_cancelarReservas);
            cancelaReserva.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isOnline(itemView.getRootView().getContext())){
                        setAlertData(itemView,getAdapterPosition());
                    }else {
                        Toast.makeText(itemView.getRootView().getContext(), "No puedes cancelar reservaciones sin conexion", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }
    }
    private void setAlertData(View itemView,int positon){
        final android.app.AlertDialog.Builder dialogo= new AlertDialog.Builder(itemView.getRootView().getContext());
        dialogo.setTitle("Cancelar Reservacion");
        dialogo.setMessage("¿Deseas cancelar esta reservacion?");
        dialogo.setCancelable(true);
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {

                    firebase.borrarDocumentReservacion(data.get(positon).getId_user(),data.get(positon).getId());
                    firebase.actualizarHorasDisponibles(data.get(positon).getId_restaurant(),
                            data.get(positon).getDate(),data.get(positon).getHour(),"true");
                    data.remove(positon);
                     notifyItemRemoved(positon);

                }catch (Exception ex){
                    Toast.makeText(itemView.getRootView().getContext(), "Upss! algo salio mal", Toast.LENGTH_LONG).show();
                }

            }
        });
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialogo.show();
    }
    public Uri cargarImagen(String img){
        try{
            File filePhoto= new File(img);
            Uri uriFile=  Uri.fromFile(filePhoto);
            return uriFile;
        }catch (Exception ex){
            ex.printStackTrace();
            Log.d("Cargar Imagen","Error al cargar Imagen "+img+"\n Mensaje "+ ex.getMessage()+ "Causa: "+ex.getCause()+" ");
            return null;
        }
    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}

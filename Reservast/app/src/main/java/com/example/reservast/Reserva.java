package com.example.reservast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.reservast.ApaterMenu.AdaptadorMenu;
import com.example.reservast.entidades.platillos;
import com.example.reservast.entidades.reservaciones;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.entidades.user;
import com.example.reservast.firestore.firebase;
import com.google.android.gms.auth.api.signin.internal.HashAccumulator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Reserva extends AppCompatActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener {
    restaurantes restaurante;
    ImageView img_background;
    TextView Tv_nombreRestaurante;
    EditText Edt_nombreReservacion;
    Button Btn_confirmaReseravcion;
    Spinner diasSpinner;
    Spinner horasSpinner;
    private firebase firebase;
    private static ArrayList<Map<String, Object>> registros= new ArrayList<Map<String, Object>>();
    private static ArrayList<String> dias= new ArrayList<String>();
    private static ArrayList<String> horas= new ArrayList<String>();
    private ArrayAdapter<CharSequence> diaAdpater;
    private ArrayAdapter<CharSequence> horaAdpater;
    private user usuario= new user();
    private reservaciones reserva= new reservaciones();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);
        if(savedInstanceState == null){
            Intent extras = getIntent();
            if(extras == null){
                restaurante = null;
            } else {
                restaurante = (restaurantes) extras.getSerializableExtra("restaurantes");
            }
        } else {
                restaurante = (restaurantes) savedInstanceState.getSerializable("restaurantes");
        }
        img_background=findViewById(R.id.Imv_backRestauranteReserva);
        Tv_nombreRestaurante=findViewById(R.id.Tv_nombreRestauranteReserva);
        diasSpinner=findViewById(R.id.Spn_dias);
        diasSpinner.setOnItemSelectedListener(this);
        horasSpinner=findViewById(R.id.Spn_horas);
        Edt_nombreReservacion=findViewById(R.id.Edt_nombreRervacion);
        Btn_confirmaReseravcion=findViewById(R.id.Btn_confirmaReseravcion);
        Btn_confirmaReseravcion.setOnClickListener(this);
        Glide.with(img_background.getContext())
                .load(restaurante.getImg())
                .centerCrop()
                .into(img_background);
        Tv_nombreRestaurante.setText(restaurante.getName());
        firebase = new firebase();
        firebase.iniciarFirebase();
        listarDatosDia();
        obtenerDatosUsuario();


    }
    public void listarDatosDia(){
        firebase.getDb().collection("dates").
                document(restaurante.getId()).
                collection("fechas")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>(){

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        registros.clear();
                        dias.clear();
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String, Object> map=document.getData();
                                registros.add(map);
                                dias.add(document.getId().toString());
                                diaAdpater=new ArrayAdapter (Reserva.this,android.R.layout.simple_spinner_item,dias);
                                diasSpinner.setAdapter(diaAdpater);
                                Log.d("SUCESS_MENU_DATE", document.getId() + " => " + document.getData());
                            }

                        } else {
                            Log.d("ERROR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
    public void listarDatosHora(String dia){
        DocumentReference docRef=firebase.getDb().collection("dates").
                document(restaurante.getId()).
                collection("fechas").
                document(dia);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        horas.clear();
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                HashMap<String,Object> map=(HashMap)document.getData();
                                for ( String key : map.keySet() ) {
                                     if(map.get(key).equals("true")){
                                         horas.add(key);
                                         horaAdpater=new ArrayAdapter (Reserva.this,android.R.layout.simple_spinner_item,horas);
                                         horasSpinner.setAdapter(horaAdpater);
                                     }
                                }
                                if(horas.size()<=0){
                                    horaAdpater=ArrayAdapter.createFromResource(Reserva.this,R.array.noexist_hours,android.R.layout.simple_spinner_item);
                                    horasSpinner.setAdapter(horaAdpater);
                                }
                                Log.d("SUCESS_DOUMENT", "DocumentSnapshot data: " + document.getData());
                            } else {
                                Log.d("NOSUCH_DOUMENT", "No such document");
                            }
                        } else {
                            Log.d("ERROR_DOUMENT", "get failed with ", task.getException());
                        }
                    }
                });
    }
    public void obtenerDatosUsuario(){
        DocumentReference docRef=firebase.getDb().collection("users").
                document(firebase.getAuth().getCurrentUser().getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        HashMap<String,Object> map=(HashMap)document.getData();
                        cargarDatos(map,document.getId().toString());
                        Log.d("SUCESS_DOUMENT", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("NOSUCH_DOUMENT", "No such document");
                    }
                } else {
                    Log.d("ERROR_DOUMENT", "get failed with ", task.getException());
                }
            }
        });
    }
    public void cargarDatos(HashMap<String,Object> map,String id){
        usuario.setName(map.get("name").toString());
        usuario.setLastname(map.get("lastname").toString());
        usuario.setEmail(map.get("email").toString());
        usuario.setImg_profile(map.get("img_profile").toString());
        usuario.setId(id);
        Edt_nombreReservacion.setText(usuario.getName()+ " "+usuario.getLastname());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (adapterView.getId()){
            case R.id.Spn_dias:
                Log.d("ENTRO_Entro", "get failed with ");
                 listarDatosHora(diasSpinner.getItemAtPosition(position).toString());
                 break;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    private void setAlertData(){
        final android.app.AlertDialog.Builder dialogo= new AlertDialog.Builder(this);
        dialogo.setTitle("Confirmar Reservacion");
        dialogo.setMessage("¿Deseas realizar esta reservacion?");
        dialogo.setCancelable(true);
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    Map<String, Object> map=reserva.getparameters(reserva);
                    try {
                        firebase.crearDocumentReservacion(usuario.getId(),restaurante.getId(),map);
                        firebase.actualizarHorasDisponibles(restaurante.getId(),reserva.getDate(),reserva.getHour(),"false");
                        Toast.makeText(Reserva.this,"Reservacion hecha",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Reserva.this,Principal.class);
                        startActivity(intent);
                    }catch (Exception ex){
                        Toast.makeText(Reserva.this,"Algo salio mal",Toast.LENGTH_LONG).show();

                    }

            }
        });
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialogo.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_confirmaReseravcion:
                if( Edt_nombreReservacion.getText().toString().trim().equals("") ||
                        diasSpinner.getSelectedItemPosition()==(-1) || horasSpinner.getSelectedItemPosition()==(-1)
                ) {
                    Toast.makeText(this,"Existen campos vacios",Toast.LENGTH_LONG).show();
                }else if(horasSpinner.getSelectedItem().toString().equals("No existen horarios dipsonibles")){
                    Toast.makeText(this,"No hay horarios disponibles en esta fecha",Toast.LENGTH_LONG).show();
                }else{
                    reserva.setName_reservation(Edt_nombreReservacion.getText().toString());
                    reserva.setDate(diasSpinner.getSelectedItem().toString());
                    reserva.setHour(horasSpinner.getSelectedItem().toString());
                    reserva.setRestaurant(restaurante.getName());
                    reserva.setImg(restaurante.getMiniature());
                    reserva.setId_restaurant(restaurante.getId());
                    reserva.setId_user(usuario.getId());
                    if(isOnline(this)){
                        setAlertData();
                    }else{
                        Toast.makeText(this,"No pudes reservar sin conexion",Toast.LENGTH_LONG).show();
                    }

                }
                break;
        }

    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}
package com.example.reservast.ApaterMenu;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.reservast.Menu;
import com.example.reservast.R;
import com.example.reservast.entidades.platillos;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.ui.home.RestaurantesAdpatador;

import java.io.File;
import java.util.ArrayList;

public class AdaptadorMenu  extends BaseAdapter {

    private Context context;
    private ArrayList<platillos> items;
    public AdaptadorMenu(Context context,ArrayList<platillos> items) {
        this.context = context;
        this.items=items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public platillos getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);

        }
        final platillos itemplatillo=getItem(i);
        ImageView imgPlatillo;
        TextView tvnombrePlatillo;
        TextView tvprecioPlatillo;
        imgPlatillo = (ImageView) view.findViewById(R.id.Imv_imagenPlatillo);
        tvnombrePlatillo = (TextView) view.findViewById(R.id.Tv_nombrePlatillo);
        tvprecioPlatillo=(TextView) view.findViewById(R.id.Tv_precioPlatillo);
        Uri image=cargarImagen(itemplatillo.getImg());
        if(image!=null){
            Glide.with(imgPlatillo.getContext())
                    .load(itemplatillo.getImg())
                    .apply(new RequestOptions().override(300, 200))
                    .into(imgPlatillo);
        }
        tvnombrePlatillo.setText(itemplatillo.getName());
        tvprecioPlatillo.setText("$ "+itemplatillo.getPrice());

        return view;
    }

    public Uri cargarImagen(String img){
        try{
            File filePhoto= new File(img);
            Uri uriFile=  Uri.fromFile(filePhoto);
            return uriFile;
        }catch (Exception ex){
            ex.printStackTrace();
            Log.d("Cargar Imagen","Error al cargar Imagen "+img+"\n Mensaje "+ ex.getMessage()+ "Causa: "+ex.getCause()+" ");
            return null;
        }
    }
}

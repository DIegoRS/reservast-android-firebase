package com.example.reservast.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reservast.R;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.firestore.firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private static ArrayList<restaurantes> registros= new ArrayList<restaurantes>();
    private RestaurantesAdpatador restauranteAdpater;
    private firebase firebase;
    private RecyclerView list;
    private GridLayoutManager glm;
    private ProgressBar prb_cragandoRestaurantes;
    private SearchView search;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        list=(RecyclerView) root.findViewById(R.id.Rcv_listaRestaurantes);
        search=(SearchView) root.findViewById(R.id.Sev_bucaRestaurantes);
        prb_cragandoRestaurantes=(ProgressBar) root.findViewById(R.id.Prb_cragandoRestaurantes);
        glm = new GridLayoutManager(root.getContext(), 1);
        list.setLayoutManager(glm);
        firebase = new firebase();
        firebase.iniciarFirebase();
        listarDatos();
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                restauranteAdpater.getFilter().filter(s);
                return false;
            }
        });

        return root;
    }
    public void listarDatos(){
        firebase.getDb().collection("restaurants")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>(){

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        registros.clear();
                        prb_cragandoRestaurantes.setVisibility(View.VISIBLE);
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                restaurantes restaurante=document.toObject(restaurantes.class);
                                registros.add(restaurante);
                                restauranteAdpater= new RestaurantesAdpatador(registros);
                                list.setAdapter(restauranteAdpater);
                                Log.d("SUCESS", document.getId() + " => " + document.getData());
                            }
                            prb_cragandoRestaurantes.setVisibility(View.GONE);
                        } else {
                            Log.d("ERROR", "Error getting documents: ", task.getException());
                        }
                    }

                });
    }
}
package com.example.reservast.ui.dashboard;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.reservast.Login;
import com.example.reservast.MainActivity;
import com.example.reservast.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.example.reservast.BuildConfig;
import com.example.reservast.Reserva;
import com.example.reservast.SingUp;
import com.example.reservast.entidades.user;
import com.example.reservast.firestore.firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

public class DashboardFragment extends Fragment implements View.OnClickListener{

    private DashboardViewModel dashboardViewModel;
    ImageView img_restaurante;
    ImageView img_profile;
    EditText Tv_Name;
    EditText Tv_lastnName;
    EditText Tv_email;
    ImageButton Imb_Editar;
    ImageView Imb_Profile;
    private Uri photoURI;
    public  static String currentPhotoPath;
    public static final int REQUEST_TAKE_PHOTO=1;
    public static final int REQUEST_TAKE_GLLERY=2;
    private firebase firebase;
    private user usuario= new user();
    private  String AUX_IMAGE;
    private ProgressBar progresProfile;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        setHasOptionsMenu(true);
        Toolbar toolbar =(Toolbar) root.findViewById(R.id.my_topAppBar);
       ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        if(ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},0);

        }

        img_restaurante=root.findViewById(R.id.Imv_backRestaurante);
        img_profile=root.findViewById(R.id.Imv_profile);
        Tv_Name=root.findViewById(R.id.Edt_nombrePersona);
        Tv_lastnName=root.findViewById(R.id.Edt_apellidosPersona);
        Tv_email=root.findViewById(R.id.Edt_correoPersona);
        Imb_Editar=root.findViewById(R.id.Imb_editar);
        Imb_Profile=root.findViewById(R.id.Imv_profile);
        Imb_Editar.setOnClickListener(this);
        Imb_Profile.setOnClickListener(this);
        progresProfile=root.findViewById(R.id.Prb_cragandoProfile);
        progresProfile.setVisibility(View.INVISIBLE);

        firebase = new firebase();
        firebase.iniciarFirebase();
        firebase.setFileReference(firebase.getStorage().getReference("img_profiles"));
        obtenerDatosUsuario();
        return root;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.top_bar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Sign_otButtonAppbar:
                firebase.getAuth().signOut();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public void cargarDatos(HashMap<String,Object> map,String id){
            usuario.setName(map.get("name").toString());
            usuario.setLastname(map.get("lastname").toString());
            usuario.setEmail(map.get("email").toString());
            usuario.setImg_profile(map.get("img_profile").toString());
            usuario.setId(id);
            AUX_IMAGE=usuario.getImg_profile();

            Tv_Name.setText(usuario.getName());
            Tv_lastnName.setText(usuario.getLastname());
            Tv_email.setText(usuario.getEmail());
            Tv_email.setEnabled(false);
            Glide.with(img_profile.getContext())
                    .load(usuario.getImg_profile())
                    .centerCrop()
                    .into(img_profile);

    }

    public void obtenerDatosUsuario(){
        DocumentReference docRef=firebase.getDb().collection("users").
                document(firebase.getAuth().getCurrentUser().getUid());
        progresProfile.setVisibility(View.VISIBLE);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        HashMap<String,Object> map=(HashMap)document.getData();
                        cargarDatos(map,document.getId().toString());
                        progresProfile.setVisibility(View.GONE);
                        Log.d("SUCESS_DOUMENT", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("NOSUCH_DOUMENT", "No such document");
                    }
                } else {
                    Log.d("ERROR_DOUMENT", "get failed with ", task.getException());
                }
            }
        });
    }

    private void setImage(){
        final android.app.AlertDialog.Builder dialogo= new AlertDialog.Builder(getContext());
        dialogo.setTitle("Recurso");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Galeria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent initentGaleria = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                initentGaleria.setType("image/");
                startActivityForResult(initentGaleria,REQUEST_TAKE_GLLERY);
            }
        });
        dialogo.setNegativeButton("Camara", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mCapture();
            }
        });
        dialogo.show();
    }
    private void mCapture(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImage();
                //galleryAdddpic();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getContext(),ex.toString(), Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        BuildConfig.APPLICATION_ID + ".fileprovider", photoFile);
                Log.i("info",photoURI.getPath().toString());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO);
            }
        }

    }
    private File createImage() throws IOException, IOException {
        String timestamp = new SimpleDateFormat("yyyyMMddHH:mm:ss").format(new Date());
        String imagefile = "_"+timestamp+"_";
        File storagedir=getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        // File storagedir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imagefile,".jpg",storagedir);
        currentPhotoPath = image.getAbsolutePath();
        Toast.makeText(getContext(),currentPhotoPath, Toast.LENGTH_LONG).show();
        return image;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode,Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            img_profile.setImageBitmap(imageBitmap);
            try{
                //ivFoto.setImageURI(photoURI);
                usuario.setImg_profile(currentPhotoPath);
                Toast.makeText(getContext(),"img: "+usuario.getImg_profile(), Toast.LENGTH_LONG).show();

            }catch(Exception ex){
                Toast.makeText(getContext(),"fallo onActivityResult "+usuario.getImg_profile(), Toast.LENGTH_LONG).show();
            }
        }
        if(requestCode == REQUEST_TAKE_GLLERY && resultCode == Activity.RESULT_OK){
            Uri imagegaleria=data.getData();
            img_profile.setImageURI(imagegaleria);
            File photoFileGaleria= null;
            Uri phtoURIGaleria=null;

            try {
                photoFileGaleria = createImage();
                usuario.setImg_profile(photoFileGaleria.getAbsolutePath());

                Log.i("info",usuario.getImg_profile());
                Log.i("info",getPath(data.getData()));

                copyFile(new File(getPath(data.getData())),new File(usuario.getImg_profile()));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContext().getApplicationContext().getContentResolver().query(uri, projection, null, null, null);
        getActivity().startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Imb_editar:
                if( Tv_Name.getText().toString().trim().equals("") || Tv_lastnName.getText().toString().trim().equals("")
                        || Tv_email.getText().toString().trim().equals("")
                        || usuario.getImg_profile().trim().equals("")
                ) {
                    Toast.makeText(getContext(),"Existen campos vacios",Toast.LENGTH_LONG).show();
                }else{
                    usuario.setName(Tv_Name.getText().toString());
                    usuario.setLastname(Tv_lastnName.getText().toString());
                    usuario.setEmail(Tv_email.getText().toString());
                    try{
                        if(isOnline(getContext())) {
                                if(!AUX_IMAGE.equals("https://firebasestorage.googleapis.com/v0/b/reservast-" +
                                        "firebse.appspot.com/o/img_profiles%2Fperosn_food.jpg?alt=media&token=629c6497-ff95-4c36-8f39-94d204722385")){
                                    firebase.borrarImgen(AUX_IMAGE);
                                    firebase.subirImagen(usuario);
                                }


                            Map<String, Object> map=usuario.getparameters(usuario);
                            firebase.actualizarDocumentUser(usuario.getId(),map);

                            Toast.makeText(getContext(),"Datos Actuzalizados",Toast.LENGTH_LONG).show();

                        }else{
                            Toast.makeText(getContext(), "No puedes actualizar datos sin conexion", Toast.LENGTH_LONG).show();
                        }



                    }catch (Exception ex){
                        Toast.makeText(getContext(),"Error de Actualizacion",Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case R.id.Imv_profile:
                setImage();
                break;
        }

    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
}
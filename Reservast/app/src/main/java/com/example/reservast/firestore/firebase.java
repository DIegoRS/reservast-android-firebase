package com.example.reservast.firestore;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.reservast.entidades.user;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.Map;

public class firebase {
    private FirebaseFirestore db;
    private FirebaseStorage storage;
    private StorageReference fileReference;
    private FirebaseAuth auth;


    public void iniciarFirebase(){
       db= FirebaseFirestore.getInstance();
       setStorage(FirebaseStorage.getInstance());
       setAuth(FirebaseAuth.getInstance());

    }
    public void crearDocumentReservacion(String idDocumentUser,String idCollectionRestaurant,Map<String, Object> map){
        getDb().collection("reservas")
                .document(idDocumentUser)
                .collection("user_reservations")
                .document().set(map);
    }
    public void borrarDocumentReservacion(String idDocumentUser,String idDocumentReservacion){
        getDb().collection("reservas")
                .document(idDocumentUser)
                .collection("user_reservations")
                .document(idDocumentReservacion)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("SUCESS_DELETE", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FAIL_DELETE", "Error deleting document", e);
                    }
                });

    }
    public void crearDocuemntUser(Map<String,Object> map,String id){
        getDb().collection("users").document(id).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("SUCESS_CREATE", "DocumentSnapshot successfully created!");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w("FAIL_CREATE", "Error created document", e);
            }
        });
    }
    public void actualizarDocumentUser(String idDocument,Map<String, Object> map){
        getDb().collection("users")
                .document(idDocument)
                .update(map);
    }
    public void actualizarHorasDisponibles(String idDocumentRestaurant,String idDocumentDate,String hora,String estado){
        getDb().collection("dates")
                .document(idDocumentRestaurant)
                .collection("fechas")
                .document(idDocumentDate)
                .update(hora,estado);
    }
    public void subirImagen(user usuario){
        StorageReference ImagesRef = getFileReference().child(usuario.getImg_profile());
        ImagesRef.putFile(cargarImagen(usuario.getImg_profile())).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Task<Uri> downloadUri = taskSnapshot.getStorage().getDownloadUrl();
                if(downloadUri.isSuccessful()){
                    usuario.setImg_profile(downloadUri.getResult().toString());
                }
                Log.d("SUCESS_Load_IMAGE", usuario.getImg_profile().toString());
            }
        });


    }
    public void borrarImgen(String name){
        StorageReference desertRef = getFileReference().child(name);
        // Delete the file
        desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("SUCESS_DELETE_IMAGE", "DocumentSnapshot successfully created!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });
    }
    private Uri cargarImagen(String img){
        try{
            File filePhoto= new File(img);
            Uri uriFile=  Uri.fromFile(filePhoto);
            return uriFile;
        }catch (Exception ex){
            ex.printStackTrace();
            Log.d("Cargar Imagen","Error al cargar Imagen "+img+"\n Mensaje "+ ex.getMessage()+ "Causa: "+ex.getCause()+" ");
            return null;
        }
    }


    public FirebaseFirestore getDb() {
        return db;
    }

    public void setDb(FirebaseFirestore db) {
        this.db = db;
    }

    public FirebaseStorage getStorage() {
        return storage;
    }

    public void setStorage(FirebaseStorage storage) {
        this.storage = storage;
    }

    public StorageReference getFileReference() {
        return fileReference;
    }

    public void setFileReference(StorageReference fileReference) {
        this.fileReference = fileReference;
    }

    public FirebaseAuth getAuth() {
        return auth;
    }

    public void setAuth(FirebaseAuth auth) {
        this.auth = auth;
    }

}

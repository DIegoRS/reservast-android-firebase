package com.example.reservast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.reservast.ApaterMenu.AdaptadorMenu;
import com.example.reservast.entidades.platillos;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.firestore.firebase;
import com.example.reservast.ui.home.RestaurantesAdpatador;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Menu extends AppCompatActivity {


    private static ArrayList<platillos> registros= new ArrayList<platillos>();
    private AdaptadorMenu adaptadorMenu;
    private firebase firebase;
    private GridView list;
    private GridLayoutManager glm;
    ImageView img_background;
    TextView Tv_nombreRestaurante;
    ImageButton btn_location;
    Button btn_reservar;
    restaurantes restaurante;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        if(savedInstanceState == null){
            Intent extras = getIntent();
            if(extras == null){
                restaurante = null;
            } else {
                restaurante = (restaurantes) extras.getSerializableExtra("restaurantes");
            }
        } else {
            restaurante = (restaurantes) savedInstanceState.getSerializable("restaurantes");
        }
        btn_location=findViewById(R.id.Imb_contactoRestaurante);
        btn_reservar=findViewById(R.id.Imb_reservarRestaurante);
        btn_reservar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context,Reserva.class);
                intent.putExtra("restaurantes",restaurante);
                context.startActivity(intent);
            }
        });
        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context,dataRestaurant.class);
                intent.putExtra("restaurantes",restaurante);
                context.startActivity(intent);
            }
        });

        img_background=findViewById(R.id.Imv_backRestauranteMenu);
        Tv_nombreRestaurante=findViewById(R.id.Tv_nombreRestauranteMenu);
        Glide.with(img_background.getContext())
                .load(restaurante.getImg())
                .centerCrop()
                .into(img_background);
        Tv_nombreRestaurante.setText(restaurante.getName());
        list = (GridView) findViewById(R.id.grid_viewPlatillos);
        firebase = new firebase();
        firebase.iniciarFirebase();
        listarDatos();


    }
    public void listarDatos(){
        Log.d("SUCESS_MENU", restaurante.getName());
        firebase.getDb().collection("dishes").
                document(restaurante.getId()).
                collection("menu")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>(){

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        registros.clear();
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                platillos platillo=document.toObject(platillos.class);
                                registros.add(platillo);
                                adaptadorMenu= new AdaptadorMenu(Menu.this,registros);
                                list.setAdapter(adaptadorMenu);
                                Log.d("SUCESS_MENU", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.d("ERROR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
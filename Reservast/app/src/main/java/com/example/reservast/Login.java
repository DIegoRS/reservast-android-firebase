package com.example.reservast;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.reservast.entidades.user;
import com.example.reservast.firestore.firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class Login extends AppCompatActivity {

    private EditText correoUsuario;
    private EditText passwordUsuario;
    private Button sing_in;
    private user usuario;
    private firebase firebase;
    private ProgressBar progresLoginUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        correoUsuario=findViewById(R.id.Edt_emailUsuarioLogin);
        passwordUsuario=findViewById(R.id.Edt_passswordUsuarioLogin);
        progresLoginUp=findViewById(R.id.Prb_cragandoLoginUp);
        progresLoginUp.setVisibility(View.INVISIBLE);
        sing_in=findViewById(R.id.Btn_SignInLogin);
        usuario=new user();
        firebase= new firebase();
        firebase.iniciarFirebase();
        passwordUsuario.setHint("Contraseña");
        passwordUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    passwordUsuario.setHint("");
                } else {
                    passwordUsuario.setHint("Contraseña");
                }
            }
        });
        sing_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(correoUsuario.getText().toString().trim().equals("") || passwordUsuario.getText().toString().trim().equals("")){
                    Toast.makeText(Login.this,"Debes introducir corrreo y contraseña", Toast.LENGTH_SHORT).show();
                }else{
                    Log_in();
                }
            }
        });
    }
    public void Log_in(){
        usuario.setEmail(correoUsuario.getText().toString());
        usuario.setPassword(passwordUsuario.getText().toString());
        progresLoginUp.setVisibility(View.VISIBLE);
        firebase.getAuth().signInWithEmailAndPassword(usuario.getEmail(),usuario.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progresLoginUp.setVisibility(View.GONE);
                            Intent intent = new Intent(Login.this,Principal.class);
                            startActivity(intent);
                            finish();
                        }else {
                            progresLoginUp.setVisibility(View.GONE);
                            Toast.makeText(Login.this,task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e.getLocalizedMessage().equals("auth/user-not-found")){
                    Toast.makeText(Login.this,"El usuario no existe", Toast.LENGTH_SHORT).show();
                }else if(e.getLocalizedMessage().equals("auth/wrong-password")){
                    Toast.makeText(Login.this,"La contraseña es incorrecta", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
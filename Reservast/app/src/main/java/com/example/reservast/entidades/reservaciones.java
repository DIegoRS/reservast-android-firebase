package com.example.reservast.entidades;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class reservaciones {
    private String name_reservation;
    private String date;
    private String hour;
    private String restaurant;
    private String img;
    private String id_restaurant;
    private String id_user;
    private String id;

    public String getName_reservation() {
        return name_reservation;
    }

    public void setName_reservation(String name_reservation) {
        this.name_reservation = name_reservation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }
    public Map<String, Object> getparameters(Object obj) {
        Map<String, Object> map = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            if(!field.getName().toString().equals("id")){
                field.setAccessible(true);
                try { map.put(field.getName(), field.get(obj)); } catch (Exception e) { }
            }

        }
        return map;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId_restaurant() {
        return id_restaurant;
    }

    public void setId_restaurant(String id_restaurant) {
        this.id_restaurant = id_restaurant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }
}

package com.example.reservast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.reservast.firestore.firebase;
import com.google.firebase.firestore.FirebaseFirestoreSettings;


public class MainActivity extends AppCompatActivity {
    private Button Log,Sing;
    private firebase firebase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log=findViewById(R.id.Btn_LoginIn);
        Sing=findViewById(R.id.Btn_SignIn);
        //iniciamos la base de  datos de firebase
        firebase= new firebase();
        firebase.iniciarFirebase();
        //Configuramos la persistencia de datos
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        firebase.getDb().setFirestoreSettings(settings);
        //verificamos si el ususrio esta en linea
        if(!isOnline(this)) {
            Toast.makeText(MainActivity.this, "Sin conexion", Toast.LENGTH_LONG).show();
        }
        //setemaos los metodos de onclik para los botones
        Log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Login.class);
                startActivity(intent);
            }
        });
        Sing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SingUp.class);
//                Toast.makeText(MainActivity.this, "Sesión Correcta", Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });


    }
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(firebase.getAuth().getCurrentUser() != null){
            Intent intent = new Intent(MainActivity.this,Principal.class);
            startActivity(intent);
            finish();
        }
    }


}
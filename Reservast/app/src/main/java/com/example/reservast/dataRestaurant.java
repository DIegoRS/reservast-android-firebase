package com.example.reservast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.reservast.entidades.restaurantes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class dataRestaurant extends AppCompatActivity implements OnMapReadyCallback  {
     private restaurantes restaurante;
     private TextView tv_nombreRestauranteData;
    private TextView tv_direccionRestauranteData;
    private TextView tv_redesRestauranteData;
    private ImageView imv_restaruranteData;
    boolean isPermissionGranter;
    //MapView mapView;
    GoogleMap googleMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_restaurant);
        chceckPermission();
        if(isPermissionGranter){
            if(checkGooglePlayService()){
                /*mapView.getMapAsync(this);
                mapView.onCreate(savedInstanceState);*/
                SupportMapFragment supportMapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapView_restaurante);
                supportMapFragment.getMapAsync(this);
            }else
            {
                Toast.makeText(this, "Google PlayServices NOT Available",Toast.LENGTH_SHORT).show();
            }

        }

        if(savedInstanceState == null){
            Intent extras = getIntent();
            if(extras == null){
                restaurante = null;
            } else {
                restaurante = (restaurantes) extras.getSerializableExtra("restaurantes");
            }
        } else {
            restaurante = (restaurantes) savedInstanceState.getSerializable("restaurantes");
        }
        tv_nombreRestauranteData=findViewById(R.id.Tv_nombreRestauranteData);
        imv_restaruranteData=findViewById(R.id.Imv_backRestauranteData);
        tv_direccionRestauranteData=findViewById(R.id.direccionRestaurante);
        tv_redesRestauranteData=findViewById(R.id.redesSociles);
        Glide.with(imv_restaruranteData.getContext())
                .load(restaurante.getImg())
                .centerCrop()
                .into(imv_restaruranteData);
        tv_nombreRestauranteData.setText(restaurante.getName());
        tv_direccionRestauranteData.setText("Correo: "+ restaurante.getEmail()+"\nTelefono: "+restaurante.getPhone());
        tv_redesRestauranteData.setText(restaurante.getSocial_net());


    }

    private boolean checkGooglePlayService(){
        GoogleApiAvailability googleApiAvailability= GoogleApiAvailability.getInstance();
        int result=googleApiAvailability.isGooglePlayServicesAvailable(this);
        if(result== ConnectionResult.SUCCESS){
            return true;
        }else if(googleApiAvailability.isUserResolvableError(result))
        {
            Dialog dialog=googleApiAvailability.getErrorDialog(this, result, 201, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    Toast.makeText(dataRestaurant.this,"User Cancelked Dialoge", Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();
        }
        return false;
    }

    private void chceckPermission(){
        Dexter.withContext(this).withPermission(Manifest.permission.ACCESS_FINE_LOCATION).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                isPermissionGranter=true;
                Toast.makeText(dataRestaurant.this, "Permission Granter", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                Intent intent= new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri= Uri.fromParts("package", getPackageName(),"");
                intent.setData(uri);
                startActivity(intent);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.cancelPermissionRequest();
            }
        }).check();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        googleMap=googleMap;
        float latitud=Float.parseFloat(restaurante.getLatitud().split(";")[0]);
        float longitud=Float.parseFloat(restaurante.getLatitud().split(";")[1]);

        LatLng latLng= new LatLng(latitud, longitud);
        MarkerOptions markerOptions= new MarkerOptions();
        markerOptions.title("My position");
        markerOptions.position(latLng);
        googleMap.addMarker(markerOptions);
        CameraUpdate cameraUpdate= CameraUpdateFactory.newLatLngZoom(latLng,15);
        googleMap.moveCamera(cameraUpdate);
        googleMap.animateCamera(cameraUpdate);
    }

}
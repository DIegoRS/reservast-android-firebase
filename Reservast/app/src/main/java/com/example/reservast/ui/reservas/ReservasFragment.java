package com.example.reservast.ui.reservas;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reservast.AdaptadorReservas.AdaptadorReservas;
import com.example.reservast.R;
import com.example.reservast.entidades.reservaciones;
import com.example.reservast.entidades.restaurantes;
import com.example.reservast.firestore.firebase;
import com.example.reservast.ui.home.RestaurantesAdpatador;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ReservasFragment extends Fragment {

    private ReservasViewModel notificationsViewModel;
    private static ArrayList<reservaciones> registros= new ArrayList<reservaciones>();
    private AdaptadorReservas reservacionAdpater;
    private firebase firebase;
    private RecyclerView list;
    private GridLayoutManager glm;
    private ProgressBar prb_cragandoResservas;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(ReservasViewModel.class);
        View root = inflater.inflate(R.layout.fragment_reservas, container, false);

        list=(RecyclerView) root.findViewById(R.id.Rcv_listaReservaciones);
        prb_cragandoResservas=(ProgressBar) root.findViewById(R.id.Prb_cragandoReservaciones);
        glm = new GridLayoutManager(root.getContext(), 1);
        list.setLayoutManager(glm);
        firebase = new firebase();
        firebase.iniciarFirebase();
        listarDatos();

        /**final TextView textView = root.findViewById(R.id.text_notifications);
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });**/
        return root;
    }
    public void listarDatos(){
        firebase.getDb().collection("reservas")
                .document(firebase.getAuth().getCurrentUser().getUid())
                .collection("user_reservations")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>(){

                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        registros.clear();
                        prb_cragandoResservas.setVisibility(View.VISIBLE);
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                reservaciones reservacion=document.toObject(reservaciones.class);
                                reservacion.setId(document.getId().toString());
                                registros.add(reservacion);
                                reservacionAdpater= new AdaptadorReservas(registros);
                                list.setAdapter(reservacionAdpater);
                                Log.d("SUCESS", document.getId() + " => " + document.getData());
                            }
                            prb_cragandoResservas.setVisibility(View.GONE);
                        } else {
                            Log.d("ERROR", "Error getting documents: ", task.getException());
                        }
                    }

                });
    }
}
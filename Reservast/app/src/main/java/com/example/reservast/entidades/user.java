package com.example.reservast.entidades;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class user {
    private String name;
    private String lastname;
    private String email;
    private String img_profile;
    private String id;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImg_profile() {
        return img_profile;
    }

    public void setImg_profile(String img_profile) {
        this.img_profile = img_profile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public Map<String, Object> getparameters(Object obj) {
        Map<String, Object> map = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if(!field.getName().toString().equals("id") &&  !field.getName().toString().equals("password")){
                try { map.put(field.getName(), field.get(obj)); } catch (Exception e) { }
            }
        }
        return map;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

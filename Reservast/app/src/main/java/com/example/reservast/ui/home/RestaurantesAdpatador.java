package com.example.reservast.ui.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.reservast.Menu;
import com.example.reservast.entidades.restaurantes;

import java.io.File;
import java.util.ArrayList;
import com.example.reservast.R;

public class RestaurantesAdpatador extends RecyclerView.Adapter<RestaurantesAdpatador.RestauranteViewHolder>
        implements Filterable {
    private ArrayList<restaurantes> data;
    private ArrayList<restaurantes> dataFull;

    public RestaurantesAdpatador(ArrayList<restaurantes> data) {
        this.data = data;
        this.dataFull=new ArrayList<restaurantes>(data);

    }

    @Override
    public RestauranteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RestaurantesAdpatador.RestauranteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_restaurantes, parent, false));
    }

    @Override
    public void onBindViewHolder(RestauranteViewHolder holder, int position) {
        restaurantes restaurante = data.get(position);
        Uri image=cargarImagen(restaurante.getMiniature());
        if(image!=null){
            Glide.with(holder.imgRestaurante.getContext())
                    .load(restaurante.getMiniature())
                    .apply(new RequestOptions().override(300, 200))
                    .into(holder.imgRestaurante);
        }
        holder.tvnombreRestaurante.setText(restaurante.getName());
        holder.tvdescription.setText(restaurante.getDescription());
        holder.calificacion.setRating(Float.parseFloat(restaurante.getRating()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        return restaurantfilter;
    }

    class RestauranteViewHolder extends RecyclerView.ViewHolder{

        ImageView imgRestaurante;
        TextView tvnombreRestaurante;
        TextView tvdescription;
        Button reservar;
        RatingBar calificacion;

        public RestauranteViewHolder(View itemView) {
            super(itemView);
            imgRestaurante = (ImageView) itemView.findViewById(R.id.Imv_imagenRestaurante);
            tvnombreRestaurante = (TextView) itemView.findViewById(R.id.Tv_nombreRestaurante);
            tvdescription= (TextView) itemView.findViewById(R.id.Tv_descripcionRestaurante);
            reservar=(Button) itemView.findViewById(R.id.Btn_reservar);
            calificacion=(RatingBar)itemView.findViewById(R.id.Rtb_Calificacion);

            reservar.setOnClickListener((view)-> {
                Context context = view.getContext();
                Intent intent = new Intent(context,Menu.class);
                intent.putExtra("restaurantes",data.get(getAdapterPosition()));
                context.startActivity(intent);
            });

        }
    }
    private Filter restaurantfilter= new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<restaurantes> filterlist=new ArrayList<>();
            if(constraint==null|| constraint.length()==0){
                filterlist.addAll(dataFull);
            }
            else{
                String pattrn=constraint.toString().toLowerCase().trim();
                for(restaurantes item :dataFull){
                    if(item.getName().toLowerCase().contains(pattrn)){
                        filterlist.add(item);
                    }
                }
            }
            FilterResults filterResults=new FilterResults();
            filterResults.values=filterlist;
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((ArrayList)results.values);
            notifyDataSetChanged();
        }
    };
    public Uri cargarImagen(String img){
        try{
            File filePhoto= new File(img);
            Uri uriFile=  Uri.fromFile(filePhoto);
            return uriFile;
        }catch (Exception ex){
            ex.printStackTrace();
            Log.d("Cargar Imagen","Error al cargar Imagen "+img+"\n Mensaje "+ ex.getMessage()+ "Causa: "+ex.getCause()+" ");
            return null;
        }
    }
}

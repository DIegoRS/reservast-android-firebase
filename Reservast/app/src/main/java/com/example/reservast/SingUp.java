package com.example.reservast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.reservast.entidades.user;
import com.example.reservast.firestore.firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class SingUp extends AppCompatActivity {
    private EditText nombreUsuario;
    private EditText apellidoUsuario;
    private EditText correoUsuario;
    private EditText passwordUsuario;
    private Button sing_up;
    private ProgressBar progresSingUp;
    private user usuario;
    private firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);

        nombreUsuario=findViewById(R.id.Edt_nombreUsusario);
        apellidoUsuario=findViewById(R.id.Edt_apellidosUsusario);
        correoUsuario=findViewById(R.id.Edt_correoUsusario);
        passwordUsuario=findViewById(R.id.Edt_passwordUsusario);
        sing_up=findViewById(R.id.Btn_SignInUp);
        progresSingUp=findViewById(R.id.Prb_cragandoSingUp);
        progresSingUp.setVisibility(View.INVISIBLE);
        firebase= new firebase();
        usuario= new user();
        firebase.iniciarFirebase();

        sing_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nombreUsuario.getText().toString().trim().equals("") || apellidoUsuario.getText().toString().trim().equals("")
                     || correoUsuario.getText().toString().trim().equals("") || passwordUsuario.getText().toString().trim().equals("")){
                        Toast.makeText(SingUp.this,"Existen campos vacios", Toast.LENGTH_SHORT).show();
                }else{
                    if(passwordUsuario.getText().toString().length() < 6){
                        Toast.makeText(SingUp.this,"el password debe de contener almenos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }else{
                        Sing_up();
                    }

                }
            }
        });
    }
    public void Sing_up(){
        usuario.setName(nombreUsuario.getText().toString());
        usuario.setLastname(apellidoUsuario.getText().toString());
        usuario.setEmail(correoUsuario.getText().toString());
        usuario.setImg_profile("https://firebasestorage.googleapis.com/v0/b/reservast-firebse.appspot.com/" +
                "o/img_profiles%2Fperosn_food.jpg?alt=media&token=629c6497-ff95-4c36-8f39-94d204722385");
        usuario.setPassword(passwordUsuario.getText().toString());
        progresSingUp.setVisibility(View.VISIBLE);
        firebase.getAuth().createUserWithEmailAndPassword(usuario.getEmail(),usuario.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            try {
                                usuario.setId(firebase.getAuth().getCurrentUser().getUid().toString());
                                firebase.crearDocuemntUser(usuario.getparameters(usuario),usuario.getId());
                                progresSingUp.setVisibility(View.GONE);
                                Toast.makeText(SingUp.this,"Bien! , Ahora estas registrado", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SingUp.this,Login.class);
                                startActivity(intent);
                                finish();
                            } catch (Exception ex){
                                Toast.makeText(SingUp.this,"Upss! , Algo salio mal", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            progresSingUp.setVisibility(View.GONE);
                            Toast.makeText(SingUp.this,task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e.getLocalizedMessage().equals("auth/email-already-exists")){
                    Toast.makeText(SingUp.this,"El email ya existe", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}